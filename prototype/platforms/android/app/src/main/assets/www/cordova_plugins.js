cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "com.manueldeveloper.volume-buttons.volumebuttons",
      "file": "plugins/com.manueldeveloper.volume-buttons/www/volumebuttons.js",
      "pluginId": "com.manueldeveloper.volume-buttons",
      "clobbers": [
        "navigator.volumebuttons"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "com.manueldeveloper.volume-buttons": "0.0.3"
  };
});