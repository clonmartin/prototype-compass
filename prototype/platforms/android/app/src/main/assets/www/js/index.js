/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        
        window.addEventListener("volumebuttonslistener", this.onVolumeButtonsListener, false);
        window.addEventListener("deviceorientation", this.onCompassListener, true);
        //nfc.addNdefListener(this.NFCHandler, function (a) { console.log("success, ", a) }, function (a) { console.log("failed, ", a) });

        document.querySelector("#ref-but").onclick = app.setReference;
        document.querySelector("#left-but").onclick = app.setLeft;
        document.querySelector("#right-but").onclick = app.setRight;
    },

    //Function for listen keyEvents
    onVolumeButtonsListener: function(info){
        console.log("Button pressed: " + info.signal);
	},

    reference:0,
    leftLimit:0,
    rightLimit:0,
    currentGrade:0,    
    //Function for listen compass sensor
    onCompassListener: function(event) {
        // process event.alpha, event.beta and event.gamma
        app.currentGrade = Number.parseInt(event.alpha);        
        document.querySelector('#grade').innerHTML = app.currentGrade;
    },   
    setReference: function(event){
        document.querySelector('#ref').innerHTML = app.currentGrade;
        compassRef.reference = app.currentGrade;
        alert(app.currentGrade);
    },
    setLeft: function(event){
        document.querySelector('#left').innerHTML = app.currentGrade;
        app.leftLimit = app.currentGrade;
    },
    setRight: function(event){
        document.querySelector('#right').innerHTML = this.currentGrade;
        app.rightLimit = app.currentGrade;
    }, 
    
    //Function for NFC
    NFCHandler: function () {
        nfc.addTagDiscoveredListener(function (nfcEvent) {
            texto = 'connected to ' + nfc.bytesToHexString(nfcEvent.tag.id)
            // texto = nfcEvent.tag,
            //     ndefMessage = texto.ndefMessage;
            nfc.connect('android.nfc.tech.NfcA', 500).then( (a) => {
                          
                console.log(texto, a)
                
                }).catch( (a) => {
                console.log('fail ' , a)
            });
        })
      }
};

app.initialize();
